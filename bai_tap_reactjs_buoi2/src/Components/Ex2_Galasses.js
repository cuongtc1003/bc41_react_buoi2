import React, { Component } from 'react'
import Modal from './Modal'


export default class Ex2_Galasses extends Component {
    render() {
        return (
            <div style={{ backgroundImage: 'url(./glassesImage/background.jpg)' }}>
                <div className='ex_glasses'>
                    <header>
                        <div className='text-center' >
                            <h2 className='py-3'>TRY GLASSES APP ONLINE</h2>
                        </div>
                    </header>
                    <div className='container'>
                        <Modal />
                    </div>
                </div>


            </div>
        )
    }
}
