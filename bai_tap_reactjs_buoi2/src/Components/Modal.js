import React, { Component } from "react";
import dataGlasses from "../Data/dataGlasses.json";

export default class Modal extends Component {
  state = {
    glasses: {},
    class: "",
  };
  handleGlasses = (glasses) => {
    this.setState({
      glasses: glasses,
      class: "info_glasses",
    });
  };
  renderGlasses = () => {
    return dataGlasses.map((item) => {
      return (
        <a
          onClick={() => {
            this.handleGlasses(item);
          }}
          className="col-4 p-2"
        >
          <img src={item.url} alt="" />
        </a>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6  position-relative">
            <img
              style={{ height: "400px", padding: "40px 100px", zIndex: 5 }}
              src="./glassesImage/model.jpg"
              alt="1"
            />
            <div>
              <img
                className="change_glasses"
                src={this.state.glasses.url}
                alt=""
              />
              <div className={this.state.class}>
                <div className="text-left pl-3 text-white">
                  <h2>{this.state.glasses.name}</h2>
                  <h3>{this.state.glasses.price}</h3>
                </div>
              </div>
            </div>
          </div>

          <div className="col-6">
            <img
              src="./glassesImage/model.jpg"
              alt="1"
              style={{ height: "400px", padding: "40px 100px", zIndex: 5 }}
            />
          </div>
        </div>
        <div className="row glasses">{this.renderGlasses()}</div>
      </div>
    );
  }
}
